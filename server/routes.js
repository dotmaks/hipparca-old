/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var express = require('express');
var path = require('path');

module.exports = function (app) {

  // Insert routes below
  app.use('/api/sportarten', require('./api/sportart'));
  app.use('/api/sportstaetten', require('./api/sportstaette'));
  app.use('/api/vereine', require('./api/verein'));
  app.use('/api/shops', require('./api/shop'));
  app.use('/api/shopkategorie', require('./api/shopkategorie'));
  app.use('/api/bundesland', require('./api/bundesland'));
  app.use('/api/land', require('./api/land'));
  app.use('/api/termine', require('./api/termin'));
  app.use('/api/protokolle', require('./api/protokoll'));
  app.use('/api/todos', require('./api/todo'));
  app.use('/api/users', require('./api/user'));

  app.use('/auth', require('./auth'));

  //console.log('dir name: ' + __dirname);

  //app.use(express.static(__dirname + '/../uploads'));
  app.use('/static', express.static(__dirname + '/../uploads'));

  // GET /style.css etc
  //app.use(express.static(__dirname + '/public'));

  // GET /static/style.css etc.
  //app.use('/static', express.static(__dirname + '/public'));


  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
    .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function (req, res) {
      //console.log('app path: ' + app.get('appPath'));
      //console.log(path.join(__dirname, '../'+app.get('appPath')));
      //res.sendfile(app.get('appPath') + '/index.html');
      //console.log( 'index path: ' + path.join(__dirname, '../'+app.get('appPath')) );
      //res.sendFile('/index.html', { root: path.join(__dirname, '../'+app.get('appPath')) });
      //res.sendFile('/index.html', { root: app.get('appPath') });
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};
