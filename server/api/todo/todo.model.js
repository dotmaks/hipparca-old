'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TodoSchema = new Schema({
    text:                        String,
    datum:                       {type: Date, default: Date.now},
    done:                        {type: Boolean, default: false},
    marked:                      {type: Boolean, default: false},

    active:                      {type: Boolean, default: true},
});

module.exports = mongoose.model('Todo', TodoSchema);
