'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TerminSchema = new Schema({
    name:                        String,
    datum:                       { type: Date, default: Date.now },
    enddatum:                    { type: Date },

    verein:                      {type: Schema.Types.ObjectId, ref: 'Verein'},
    shop:                        {type: Schema.Types.ObjectId, ref: 'Shop'},
    kategorien:                  [String],

    sportarten:                  [{ type: Schema.Types.ObjectId, ref: 'Sportart' }],

    bild:                        String,
    strasse:                     String,
    plz:                         String,
    ort:                         String,
    bundesland:                  { type: Schema.Types.ObjectId, ref: 'Bundesland' },
    land:                        { type: Schema.Types.ObjectId, ref: 'Land' },
    koordLaenge:                 String,
    koordBreite:                 String,
    kontaktperson:               String,
    telefon:                     String,
    email:                       String,
    url:                         String,

    protokoll:                   String,
    kurzbeschreibung:            String,
    eigentext:                   String,

    active:                      {type: Boolean, default: true},
    _created:                    { type: Date, default: Date.now },
    _createdby:                  { type: Schema.Types.ObjectId, ref: 'User' },
    _lastupdate:                 { type: Date },
    _lastupdateby:               { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Termin', TerminSchema);
