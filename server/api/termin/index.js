'use strict';

var express = require('express');
var controller = require('./termin.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/next', controller.indexNext);
//router.get('/user/:email', controller.indexUser);
router.get('/user/', auth.isAuthenticated(), controller.indexUser);
router.get('/:id', controller.show);

router.post('/upload/image/:id/:filename', auth.isAuthenticated(), controller.uploadImage);
router.post('/', auth.isAuthenticated(), controller.create);

router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);

router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
