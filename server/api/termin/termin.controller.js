'use strict';

var _ = require('lodash');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var Termin = require('./termin.model');

var User = require('../user/user.model');

var S3FS = require('s3fs'),
    s3fsImpl = new S3FS('hipparca-sport', {
      accessKeyId: 'AKIAJLMNRO6M5YMQSLUQ',
      secretAccessKey: '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',
      region: 'eu-central-1',
    });


// Get list of termine
exports.index = function(req, res) {
  /*Termin.find(function (err, termine) {
     if(err) { return handleError(res, err); }
     return res.json(200, termine);
     return res.status(200).json(termine);
   });*/
  Termin.find({})
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    .populate( 'verein' )
    .populate( 'shop' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .populate( 'sportarten' )
    .exec( function (err, termine) {
      if (err) { return handleError(res, err); }
      //return res.json(200, termine);
      return res.status(200).json(termine);
    });
};

// Get list of future termine
exports.indexNext = function(req, res) {
  /*Termin.find(function (err, termine) {
   if(err) { return handleError(res, err); }
   return res.json(200, termine);
   return res.status(200).json(termine);
   });*/
  Termin.find({})
    //.where('datum').gt(new Date()) // TODO: get only future termine
    //.where('enddatum').gt(new Date())
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    .populate( 'verein' )
    .populate( 'shop' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .populate( 'sportarten' )
    .exec( function (err, termine) {
      if (err) { return handleError(res, err); }
      //return res.json(200, termine);
      return res.status(200).json(termine);
    });
};

// Get list of user termine
exports.indexUser = function(req, res) {
  if (req.body.email) {
    var email = req.body.email;
  } else if (req.user.email) {
    var email = req.user.email;
  } else {
    return handleError(res, null);
  }

  User.findOne({ email: email }, function (err, user) {
    if (user) {
      var query = { $or: [ { email: email }, { _createdby: user._id } ] };
    } else {
      var query = { email: email };
    }

    Termin.find(query)
      .populate({ path: '_createdby', select: '_id name email' })
      .populate({ path: '_lastupdateby', select: '_id name email' })
      .populate( 'verein' )
      .populate( 'shop' )
      .populate( 'bundesland' )
      .populate( 'land' )
      .populate( 'sportarten' )
      .exec(function (err, termine) {
        if (err) { return handleError(res, err); }
        //return res.json(200, termine);
        return res.status(200).json(termine);
      });
  });
};

// Get a single termin
exports.show = function(req, res) {
  /*Termin.findById(req.params.id, function (err, termin) {
   if(err) { return handleError(res, err); }
   if(!termin) { return res.sendStatus(404); }
   return res.json(termin);
   });*/
  Termin.findById(req.params.id)
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    .populate( 'verein' )
    .populate( 'shop' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .populate( 'sportarten' )
    .exec( function (err, termin) {
      if(err) { return handleError(res, err); }
      if(!termin) { return res.sendStatus(404); }
      return res.json(termin);
      //return res.status(200).json(termin);
    } );
};

// Creates a new termin in the DB.
exports.create = function(req, res) {
  var _termin = _.merge({ _createdby: req.user._id }, req.body);

  if (typeof _termin.bundesland !== 'undefined' && _termin.bundesland !== null && typeof _termin.bundesland._id !== 'undefined') { _termin.bundesland = _termin.bundesland._id; }
  if (typeof _termin.land !== 'undefined' && _termin.land !== null && typeof _termin.land._id !== 'undefined') { _termin.land = _termin.land._id; }
  if (typeof _termin.verein !== 'undefined' && _termin.verein !== null && typeof _termin.verein._id !== 'undefined') { _termin.verein = _termin.verein._id; }
  if (typeof _termin.shop !== 'undefined' && _termin.shop !== null && typeof _termin.shop._id !== 'undefined') { _termin.shop = _termin.shop._id; }

  var sportarten = _termin.sportarten;
  _termin.sportarten = [];
  _.forEach( sportarten, function(n) {
    //console.log(n);
    _termin.sportarten.push(n);
  });
  _termin.sportarten = _.uniq(_termin.sportarten);

  var kategorien = _termin.kategorien;
  _termin.kategorien = [];
  _.forEach( kategorien, function(n) {
    //console.log(n);
    _termin.kategorien.push(n);
  });
  _termin.kategorien = _.uniq(_termin.kategorien);

  Termin.create(_termin, function(err, termin) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(termin);
  });
};

// Updates an existing termin in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  if(req.body._created) { delete req.body._created; }
  if(req.body._createdby) { delete req.body._createdby; }

  Termin.findById(req.params.id, function (err, termin) {
    if (err) { return handleError(res, err); }
    if(!termin) { return res.sendStatus(404); }

    if (typeof req.body.bundesland !== 'undefined' && req.body.bundesland !== null && typeof req.body.bundesland._id !== 'undefined') { req.body.bundesland = req.body.bundesland._id; }
    if (typeof req.body.land !== 'undefined' && req.body.land !== null && typeof req.body.land._id !== 'undefined') { req.body.land = req.body.land._id; }

    var _termin = _.merge(req.body, { _lastupdate: new Date(), _lastupdateby: req.user._id });
    termin.kategorien = [];
    termin.sportarten = [];
    var updated = _.merge(termin, _termin);

    var kategorien = updated.kategorien;
    updated.kategorien = [];
    _.forEach( kategorien, function(n) {
      //console.log(n);
      updated.kategorien.push(n);
    });
    updated.kategorien = _.uniq(updated.kategorien);
    //console.log(updated);

    var sportarten = updated.sportarten;
    updated.sportarten = [];
    _.forEach(sportarten, function (n) {
      //console.log(n);
      updated.sportarten.push(n);
    });
    updated.sportarten = _.uniq(updated.sportarten);

    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, termin);
      return res.status(200).json(termin);
    });
  });
};

// Deletes a termin from the DB.
exports.destroy = function(req, res) {
  Termin.findById(req.params.id, function (err, termin) {
    if(err) { return handleError(res, err); }
    if(!termin) { return res.sendStatus(404); }
    termin.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};


// Image upload
exports.uploadImageOld = function(req, res) {
  console.log('uploading to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath)===false) {
    fs.mkdirSync(tempPath,'0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function(err, fields, files) {
    if (err) { return handleError(res, err); }

    var file = files.file[0];
    //var contentType = file.headers['content-type'];
    //var extension = file.path.substring(file.path.lastIndexOf('.'));
    var filename = file.originalFilename;
    var tempFile = file.path;
    //var destPath = 'uploads' + path.sep + req.params.id;
    //var destFile = destPath + path.sep + filename;
    var destPath = 'uploads' + path.sep + 'image';
    var destFile = destPath + path.sep + req.params.id + '_' + filename;

    var dirs = destPath.split(path.sep);
    var newDir = '';
    //console.log(__dirname);
    for (var i = 0; i < dirs.length; i++) {
      newDir += (i>0) ? path.sep : '';
      newDir += dirs[i];
      console.log(newDir);

      if (fs.existsSync(newDir)===false) {
        fs.mkdirSync(newDir,'0755');
        console.log('mkdir '+newDir);
      }
    }

    //console.log(file);
    //console.log(contentType);
    //console.log(filename);
    //console.log(tempFile);
    //console.log(destFile);

    // copy file to dir
    fs.rename(tempFile, destFile, function(err) {
      if(err) {
        console.error('rename error: '+err.stack);
        return handleError(res, err);
      } else {
        Termin.findById(req.params.id, function (err, termin) {
          //console.log( filename + ': ' + objekt.__v)
          if (err) { return handleError(res, err); }
          if (!termin) { return res.sendStatus(404); }

          termin.bild = filename;

          termin.save(function (err) {
            if (err) {  console.log('save error: ' + err); return handleError(res, err); }
            console.log('image (' + filename + ') uploaded and saved');
          });
          //return res.json(200, objekt);
          return res.status(200).end();;
        });
      }
    });

    //return;
  });

};

// Image upload S3
exports.uploadImage = function (req, res) {
  console.log('uploading (' + req.params.filename + ') to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath) === false) {
    fs.mkdirSync(tempPath, '0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function (err, fields, files) {
    if (err) {
      return handleError(res, err);
    }

    var file = files.file[0];
    //var filename = file.originalFilename;
    var filename = req.params.filename;
    var tempFile = file.path;
    //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destFile = 'image' + path.sep + req.params.id + '_' + filename;

    // hipparca-sport.s3.amazonaws.com
    var stream = fs.createReadStream(file.path);
    s3fsImpl.writeFile(destFile, stream).then(function () {
      console.log('s3fs ok');
      fs.unlink(file.path, function (err) {
        if (err) {
          console.error(err);
        }
      });

      Termin.findById(req.params.id, function (err, termin) {
        if (err) {
          return handleError(res, err);
        }
        if (!termin) {
          return res.sendStatus(404);
        }

        termin.bild = filename;
        //console.log(filename);

        termin.save(function (err) {
          if (err) {
            console.log('save error: ' + err);
            return handleError(res, err);
          }
          console.log('image (' + filename + ') uploaded and saved');
        });
        //return res.json(200, objekt);
        return res.status(200).end();
      });
      //res.status(200).end();
    });
  });

};

function handleError(res, err) {
  return res.status(500).send(err);
}
