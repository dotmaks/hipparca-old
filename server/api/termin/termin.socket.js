/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Termin = require('./termin.model.js');

exports.register = function(socket) {
  Termin.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Termin.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('termin:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('termin:remove', doc);
}