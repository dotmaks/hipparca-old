'use strict';

var _ = require('lodash');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var Verein = require('./verein.model');

var User = require('../user/user.model');

var S3FS = require('s3fs'),
    s3fsImpl = new S3FS('hipparca-sport', {
        accessKeyId: 'AKIAJLMNRO6M5YMQSLUQ',
        secretAccessKey: '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',
        region: 'eu-central-1',
    });


// Get list of vereins
exports.index = function(req, res) {
  /*Verein.find(function (err, vereins) {
    if(err) { return handleError(res, err); }
    return res.json(200, vereins);
    return res.status(200).json(vereins);
  });*/
  Verein.find({})
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    //.populate( 'sektionen' )
    .populate( 'sportarten' )
    .populate( 'kategorien' )
    .populate( 'sportstaetten' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .exec( function (err, vereine) {
      if(err) { return handleError(res, err); }
      //return res.json(200, vereine);
      return res.status(200).json(vereine);
    } );
};

// Get list of user vereine
exports.indexUser = function(req, res) {
  if (req.body.email) {
    var email = req.body.email;
  } else if (req.user.email) {
    var email = req.user.email;
  } else {
    return handleError(res, null);
  }

  User.findOne({ email: email }, function (err, user) {
    if (user) {
      var query = { $or: [ { email: email }, { _createdby: user._id } ] };
    } else {
      var query = { email: email };
    }

    Verein.find(query)
      .populate({ path: '_createdby', select: '_id name email' })
      .populate({ path: '_lastupdateby', select: '_id name email' })
      //.populate( 'sektionen' )
      .populate('sportarten')
      .populate('kategorien')
      .populate('sportstaetten')
      .populate('bundesland')
      .populate('land')
      .exec(function (err, vereine) {
        if (err) {
          return handleError(res, err);
        }
        //return res.json(200, vereine);
        return res.status(200).json(vereine);
      });
  });
};

// Get a single verein
exports.show = function(req, res) {
  /*Verein.findById(req.params.id, function (err, verein) {
    if(err) { return handleError(res, err); }
    if(!verein) { return res.sendStatus(404); }
    return res.json(verein);
  });*/
  Verein.findById(req.params.id)
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    //.populate( 'sektionen' )
    .populate( 'sportarten' )
    .populate( 'kategorien' )
    .populate( 'sportstaetten' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .exec( function (err, verein) {
      if(err) { return handleError(res, err); }
      if(!verein) { return res.sendStatus(404); }
      return res.json(verein);
      //return res.status(200).json(verein);
    } );
};

// Creates a new verein in the DB.
exports.create = function(req, res) {
  var _verein = _.merge({ _createdby: req.user._id }, req.body);

  if (typeof _verein.bundesland !== 'undefined' && _verein.bundesland !== null && typeof _verein.bundesland._id !== 'undefined') { _verein.bundesland = _verein.bundesland._id; }
  if (typeof _verein.land !== 'undefined' && _verein.land !== null && typeof _verein.land._id !== 'undefined') { _verein.land = _verein.land._id; }

  var sportarten = _verein.sportarten;
  _verein.sportarten = [];
  _.forEach( sportarten, function(n) {
    //console.log(n);
    _verein.sportarten.push(n);
  });
  _verein.sportarten = _.uniq(_verein.sportarten);

  var kategorien = _verein.kategorien;
  _verein.kategorien = [];
  _.forEach( kategorien, function(n) {
    //console.log(n);
    _verein.kategorien.push(n);
  });
  _verein.kategorien = _.uniq(_verein.kategorien);

  var sportstaetten = _verein.sportstaetten;
  _verein.sportstaetten = [];
  _.forEach( sportstaetten, function(n) {
    //console.log(n);
    _verein.sportstaetten.push(n);
  });
  _verein.sportstaetten = _.uniq(_verein.sportstaetten);

  Verein.create(_verein, function(err, verein) {
    if(err) { return handleError(res, err); }
    //return res.json(201, verein);
    return res.status(201).json(verein);
  });
};

// Updates an existing verein in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  if(req.body._created) { delete req.body._created; }
  if(req.body._createdby) { delete req.body._createdby; }

  Verein.findById(req.params.id, function (err, verein) {
    if (err) { return handleError(res, err); }
    if (!verein) { return res.sendStatus(404); }

    if (typeof req.body.bundesland !== 'undefined' && req.body.bundesland !== null && typeof req.body.bundesland._id !== 'undefined') { req.body.bundesland = req.body.bundesland._id; }
    if (typeof req.body.land !== 'undefined' && req.body.land !== null && typeof req.body.land._id !== 'undefined') { req.body.land = req.body.land._id; }

    var _verein = _.merge(req.body, { _lastupdate: new Date(), _lastupdateby: req.user._id });
    verein.sportarten = [];
    verein.kategorien = [];
    verein.sportstaetten = [];
    verein.bilder = [];
    var updated = _.merge(verein, _verein);

    var sportarten = updated.sportarten;
    updated.sportarten = [];
    _.forEach( sportarten, function(n) {
      //console.log(n);
      updated.sportarten.push(n);
    });
    updated.sportarten = _.uniq(updated.sportarten);

    var kategorien = updated.kategorien;
    updated.kategorien = [];
    _.forEach( kategorien, function(n) {
      //console.log(n);
      updated.kategorien.push(n);
    });
    updated.kategorien = _.uniq(updated.kategorien);

    var sportstaetten = updated.sportstaetten;
    updated.sportstaetten = [];
    _.forEach( sportstaetten, function(n) {
      //console.log(n);
      updated.sportstaetten.push(n);
    });
    updated.sportstaetten = _.uniq(updated.sportstaetten);

    //console.log(updated);

    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, verein);
      return res.status(200).json(verein);
    });
  });
};

// Deletes a verein from the DB.
exports.destroy = function(req, res) {
  Verein.findById(req.params.id, function (err, verein) {
    if(err) { return handleError(res, err); }
    if(!verein) { return res.sendStatus(404); }
    verein.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};


// Logo upload old
exports.uploadLogoOld = function(req, res) {
  console.log('uploading to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath)===false) {
    fs.mkdirSync(tempPath,'0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function(err, fields, files) {
    if (err) { return handleError(res, err); }

    var file = files.file[0];
    //var contentType = file.headers['content-type'];
    //var extension = file.path.substring(file.path.lastIndexOf('.'));
    var filename = file.originalFilename;
    var tempFile = file.path;
    //var destPath = 'uploads' + path.sep + req.params.id;
    //var destFile = destPath + path.sep + filename;
      //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destPath = 'uploads' + path.sep + 'logo'; // TODO: change to $OPENSHIFT_DATA_DIR
    var destFile = destPath + path.sep + req.params.id + '_' + filename;

    var dirs = destPath.split(path.sep);
    var newDir = '';
    //console.log(__dirname);
    for (var i = 0; i < dirs.length; i++) {
      newDir += (i>0) ? path.sep : '';
      newDir += dirs[i];
      console.log(newDir);

      if (fs.existsSync(newDir)===false) {
        fs.mkdirSync(newDir,'0755');
        console.log('mkdir '+newDir);
      }
    }

    //console.log(file);
    //console.log(contentType);
    //console.log(filename);
    //console.log(tempFile);
    //console.log(destFile);

    // copy file to dir
    fs.rename(tempFile, destFile, function(err) {
      if(err) {
        console.error('rename error: '+err.stack);
        return handleError(res, err);
      } else {
        Verein.findById(req.params.id, function (err, verein) {
          //console.log( filename + ': ' + objekt.__v)
          if (err) { return handleError(res, err); }
          if (!verein) { return res.sendStatus(404); }

          verein.logo = filename;

          verein.save(function (err) {
            if (err) {  console.log('save error: ' + err); return handleError(res, err); }
            console.log('logo (' + filename + ') uploaded and saved');
          });
          //return res.json(200, objekt);
          return res.status(200).end();;
        });
      }
    });

    //return;
  });

};

// Logo upload S3
exports.uploadLogo = function (req, res) {
  console.log('uploading (' + req.params.filename + ') to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath) === false) {
    fs.mkdirSync(tempPath, '0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function (err, fields, files) {
    if (err) {
      return handleError(res, err);
    }

    var file = files.file[0];
    //var filename = file.originalFilename;
    var filename = req.params.filename;
    var tempFile = file.path;
    //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destFile = 'logo' + path.sep + req.params.id + '_' + filename;

    // hipparca-sport.s3.amazonaws.com
    var stream = fs.createReadStream(file.path);
    s3fsImpl.writeFile(destFile, stream).then(function () {
      console.log('s3fs ok');
      fs.unlink(file.path, function (err) {
        if (err) {
          console.error(err);
        }
      });

      Verein.findById(req.params.id, function (err, verein) {
        if (err) {
          return handleError(res, err);
        }
        if (!verein) {
          return res.sendStatus(404);
        }

        verein.logo = filename;

        verein.save(function (err) {
          if (err) {
            console.log('save error: ' + err);
            return handleError(res, err);
          }
          console.log('logo (' + filename + ') uploaded and saved');
        });
        //return res.json(200, objekt);
        return res.status(200).end();
      });
      //res.status(200).end();
    });
  });

};


// Image upload old
exports.uploadImageOld = function(req, res) {
    console.log('uploading to ' + req.params.id);
    var tempPath = 'temp/';

    if (fs.existsSync(tempPath)===false) {
        fs.mkdirSync(tempPath,'0755');
    }

    var form = new multiparty.Form({
        autoFiles: true,
        uploadDir: '.' + path.sep + tempPath
    });

    form.parse(req, function(err, fields, files) {
        if (err) { return handleError(res, err); }

        var file = files.file[0];
        //var contentType = file.headers['content-type'];
        //var extension = file.path.substring(file.path.lastIndexOf('.'));
        var filename = file.originalFilename;
        var tempFile = file.path;
        //var destPath = 'uploads' + path.sep + req.params.id;
        //var destFile = destPath + path.sep + filename;
        var destPath = 'uploads' + path.sep + 'image';
        var destFile = destPath + path.sep + req.params.id + '_' + filename;

        var dirs = destPath.split(path.sep);
        var newDir = '';
        //console.log(__dirname);
        for (var i = 0; i < dirs.length; i++) {
            newDir += (i>0) ? path.sep : '';
            newDir += dirs[i];
            console.log(newDir);

            if (fs.existsSync(newDir)===false) {
                fs.mkdirSync(newDir,'0755');
                console.log('mkdir '+newDir);
            }
        }

        //console.log(file);
        //console.log(contentType);
        //console.log(filename);
        //console.log(tempFile);
        //console.log(destFile);

        // copy file to dir
        fs.rename(tempFile, destFile, function(err) {
            if(err) {
                console.error('rename error: '+err.stack);
                return handleError(res, err);
            } else {
                Verein.findById(req.params.id, function (err, verein) {
                    //console.log( filename + ': ' + objekt.__v)
                    if (err) { return handleError(res, err); }
                    if (!verein) { return res.sendStatus(404); }

                    verein.bilder.push(filename);

                    verein.save(function (err) {
                        if (err) {  console.log('save error: ' + err); return handleError(res, err); }
                        console.log('image (' + filename + ') uploaded and saved');
                    });
                    //return res.json(200, objekt);
                    return res.status(200).end();;
                });
            }
        });

        //return;
    });

};

// Image upload S3
exports.uploadImage = function (req, res) {
  console.log('uploading (' + req.params.filename + ') to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath) === false) {
    fs.mkdirSync(tempPath, '0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function (err, fields, files) {
    if (err) {
      return handleError(res, err);
    }

    console.log(files);
    var file = files.file[0];
    //var filename = file.originalFilename;
    var filename = req.params.filename;
    var tempFile = file.path;
    //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destFile = 'image' + path.sep + req.params.id + '_' + filename;

    // hipparca-sport.s3.amazonaws.com
    var stream = fs.createReadStream(file.path);
    s3fsImpl.writeFile(destFile, stream).then(function () {
      console.log('s3fs ok');
      fs.unlink(file.path, function (err) {
        if (err) {
          console.error(err);
        }
      });

      Verein.findById(req.params.id, function (err, verein) {
        if (err) {
          return handleError(res, err);
        }
        if (!verein) {
          return res.sendStatus(404);
        }

        verein.bilder.push(filename);
        //console.log(filename);

        verein.save(function (err) {
          if (err) {
            console.log('save error: ' + err);
            return handleError(res, err);
          }
          console.log('image (' + filename + ') uploaded and saved');
        });
        return res.status(200).end();
      });
      //res.status(200).end();
    });
  });

};


function handleError(res, err) {
  return res.status(500).send(err);
}
