'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VereinSchema = new Schema({
    vereinsname:                 String,

    protokoll:                   String,
    kurzbeschreibung:            String,
    dachorganisation:            String,
    organisation:                String,

    //sektionen:                   [{ type: Schema.Types.ObjectId, ref: 'Sportart' }],

    sportarten:                  [{ type: Schema.Types.ObjectId, ref: 'Sportart' }],
    kategorien:                  [{ type: Schema.Types.ObjectId, ref: 'Sportart' }],

    sportstaetten:               [{ type: Schema.Types.ObjectId, ref: 'Sportstaette' }],
    logo:                        String,
    strasse :                    String,
    plz:                         String,
    ort:                         String,
    bundesland:                  { type: Schema.Types.ObjectId, ref: 'Bundesland' },
    land:                        { type: Schema.Types.ObjectId, ref: 'Land' },
    koordLaenge:                 String,
    koordBreite:                 String,
    kontaktperson:               String,
    telefon:                     String,
    email:                       String,
    url:                         String,

    trainingszeiten:             String,
    kosten:                      String,
    trainer:                     String,
    kurse:                       String,
    eigentext:                   String,
    bilder:                      [String],

    active:                      { type: Boolean, default: false },
    _created:                    { type: Date, default: Date.now },
    _createdby:                  { type: Schema.Types.ObjectId, ref: 'User' },
    _lastupdate:                 { type: Date },
    _lastupdateby:               { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Verein', VereinSchema);
