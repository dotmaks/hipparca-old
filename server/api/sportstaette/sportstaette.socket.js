/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Sportstaette = require('./sportstaette.model');

exports.register = function(socket) {
  Sportstaette.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Sportstaette.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('sportstaette:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('sportstaette:remove', doc);
}