'use strict';

var _ = require('lodash');
var multiparty = require('multiparty');
var fs = require('fs');
var path = require('path');
var Sportstaette = require('./sportstaette.model');

var User = require('../user/user.model');

var S3FS = require('s3fs'),
  s3fsImpl = new S3FS('hipparca-sport', {
    accessKeyId: 'AKIAJLMNRO6M5YMQSLUQ',
    secretAccessKey: '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',
    region: 'eu-central-1',
  });


// Get list of sportstaetten
exports.index = function(req, res) {
  /*Sportstaette.find(function (err, sportstaetten) {
    if(err) { return handleError(res, err); }
    return res.json(200, sportstaetten);
    return res.status(200).json(sportstaetten);
  });*/
  Sportstaette.find({})
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    .populate( 'sportarten' )
    .populate( 'kategorien' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .exec( function (err, sportstaetten) {
      if(err) { return handleError(res, err); }
      //return res.json(200, sportstaetten);
      return res.status(200).json(sportstaetten);
    } );
};

// Get list of user sportstaetten
exports.indexUser = function(req, res) {
  if (req.body.email) {
    var email = req.body.email;
  } else if (req.user.email) {
    var email = req.user.email;
  } else {
    return handleError(res, null);
  }

  User.findOne({ email: email }, function (err, user) {
    if (user) {
      var query = { $or: [ { email: email }, { _createdby: user._id } ] };
    } else {
      var query = { email: email };
    }

    Sportstaette.find(query)
      .populate({ path: '_createdby', select: '_id name email' })
      .populate({ path: '_lastupdateby', select: '_id name email' })
      //.populate( 'sektionen' )
      .populate('sportarten')
      .populate('kategorien')
      .populate('bundesland')
      .populate('land')
      .exec(function (err, sportstaetten) {
        if (err) {
          return handleError(res, err);
        }
        //return res.json(200, sportstaetten);
        return res.status(200).json(sportstaetten);
      });
  });
};

// Get a single sportstaette
exports.show = function(req, res) {
  /*Sportstaette.findById(req.params.id, function (err, sportstaette) {
    if(err) { return handleError(res, err); }
    if(!sportstaette) { return res.sendStatus(404); }
    return res.json(sportstaette);
  });*/
  Sportstaette.findById(req.params.id)
    .populate( { path: '_createdby', select: '_id name email' } )
    .populate( { path: '_lastupdateby', select: '_id name email' } )
    .populate( 'sportarten' )
    .populate( 'kategorien' )
    .populate( 'bundesland' )
    .populate( 'land' )
    .exec( function (err, sportstaette) {
      if(err) { return handleError(res, err); }
      if(!sportstaette) { return res.sendStatus(404); }
      return res.json(sportstaette);
      //return res.status(200).json(sportstaette);
    } );
};

// Creates a new sportstaette in the DB.
exports.create = function(req, res) {
  var _sportstaette = _.merge({ _createdby: req.user._id }, req.body);

  if (typeof _sportstaette.bundesland !== 'undefined' && _sportstaette.bundesland !== null && typeof _sportstaette.bundesland._id !== 'undefined') { _sportstaette.bundesland = _sportstaette.bundesland._id; }
  if (typeof _sportstaette.land !== 'undefined' && _sportstaette.land !== null && typeof _sportstaette.land._id !== 'undefined') { _sportstaette.land = _sportstaette.land._id; }

  var sportarten = _sportstaette.sportarten;
  _sportstaette.sportarten = [];
  _.forEach( sportarten, function(n) {
    //console.log(n);
    _sportstaette.sportarten.push(n);
  });
  _sportstaette.sportarten = _.uniq(_sportstaette.sportarten);

  var kategorien = _sportstaette.kategorien;
  _sportstaette.kategorien = [];
  _.forEach( kategorien, function(n) {
    //console.log(n);
    _sportstaette.kategorien.push(n);
  });
  _sportstaette.kategorien = _.uniq(_sportstaette.kategorien);

  //_sportstaette.markModified('sportarten'); // lets mongoose know these values changed
  //_sportstaette.markModified('kategorien'); // lets mongoose know these values changed
  Sportstaette.create(_sportstaette, function(err, sportstaette) {
    if(err) { return handleError(res, err); }
    //return res.json(201, sportstaette);
    return res.status(201).json(sportstaette);
  });
};

// Updates an existing sportstaette in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  if(req.body._created) { delete req.body._created; }
  if(req.body._createdby) { delete req.body._createdby; }

  Sportstaette.findById(req.params.id, function (err, sportstaette) {
    if (err) { return handleError(res, err); }
    if(!sportstaette) { return res.sendStatus(404); }

    if (typeof req.body.bundesland !== 'undefined' && req.body.bundesland !== null && typeof req.body.bundesland._id !== 'undefined') { req.body.bundesland = req.body.bundesland._id; }
    if (typeof req.body.land !== 'undefined' && req.body.land !== null && typeof req.body.land._id !== 'undefined') { req.body.land = req.body.land._id; }

    var _sportstaette = _.merge(req.body, { _lastupdate: new Date(), _lastupdateby: req.user._id });
    sportstaette.sportarten = [];
    sportstaette.kategorien = [];
    sportstaette.bilder = [];
    var updated = _.merge(sportstaette, _sportstaette);

    var sportarten = updated.sportarten;
    updated.sportarten = [];
    _.forEach( sportarten, function(n) {
      //console.log(n);
      updated.sportarten.push(n);
    });
    updated.sportarten = _.uniq(updated.sportarten);

    var kategorien = updated.kategorien;
    updated.kategorien = [];
    _.forEach( kategorien, function(n) {
      //console.log(n);
      updated.kategorien.push(n);
    });
    updated.kategorien = _.uniq(updated.kategorien);

    //console.log(updated.bilder);

    //updated.sportarten = _.uniq(updated.sportarten, '_id');
    //updated.kategorien = _.uniq(updated.kategorien, '_id');

    //updated.markModified('sportarten'); // lets mongoose know these values changed
    //updated.markModified('kategorien'); // lets mongoose know these values changed
    //console.log(updated);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, sportstaette);
      return res.status(200).json(sportstaette);
    });
  });
};

// Deletes a sportstaette from the DB.
exports.destroy = function(req, res) {
  Sportstaette.findById(req.params.id, function (err, sportstaette) {
    if(err) { return handleError(res, err); }
    if(!sportstaette) { return res.sendStatus(404); }
    sportstaette.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};


// Logo upload
exports.uploadLogoOld = function(req, res) {
    console.log('uploading to ' + req.params.id);
    var tempPath = 'temp/';

    if (fs.existsSync(tempPath)===false) {
        fs.mkdirSync(tempPath,'0755');
    }

    var form = new multiparty.Form({
        autoFiles: true,
        uploadDir: '.' + path.sep + tempPath
    });

    form.parse(req, function(err, fields, files) {
        if (err) { return handleError(res, err); }

        var file = files.file[0];
        //var contentType = file.headers['content-type'];
        //var extension = file.path.substring(file.path.lastIndexOf('.'));
        var filename = file.originalFilename;
        var tempFile = file.path;
        //var destPath = 'uploads' + path.sep + req.params.id;
        //var destFile = destPath + path.sep + filename;
        var destPath = 'uploads' + path.sep + 'logo';
        var destFile = destPath + path.sep + req.params.id + '_' + filename;

        var dirs = destPath.split(path.sep);
        var newDir = '';
        //console.log(__dirname);
        for (var i = 0; i < dirs.length; i++) {
            newDir += (i>0) ? path.sep : '';
            newDir += dirs[i];
            console.log(newDir);

            if (fs.existsSync(newDir)===false) {
                fs.mkdirSync(newDir,'0755');
                console.log('mkdir '+newDir);
            }
        }

        //console.log(file);
        //console.log(contentType);
        //console.log(filename);
        //console.log(tempFile);
        //console.log(destFile);

        // copy file to dir
        fs.rename(tempFile, destFile, function(err) {
            if(err) {
                console.error('rename error: '+err.stack);
                return handleError(res, err);
            } else {
                Sportstaette.findById(req.params.id, function (err, sportstaette) {
                    //console.log( filename + ': ' + objekt.__v)
                    if (err) { return handleError(res, err); }
                    if (!sportstaette) { return res.sendStatus(404); }

                    sportstaette.logo = filename;

                    sportstaette.save(function (err) {
                        if (err) {  console.log('save error: ' + err); return handleError(res, err); }
                        console.log('logo (' + filename + ') uploaded and saved');
                    });
                    //return res.json(200, objekt);
                    return res.status(200).end();;
                });
            }
        });

        //return;
    });

};

// Logo upload S3
exports.uploadLogo = function (req, res) {
  console.log('uploading (' + req.params.filename + ') to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath) === false) {
    fs.mkdirSync(tempPath, '0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function (err, fields, files) {
    if (err) {
      return handleError(res, err);
    }

    var file = files.file[0];
    //var filename = file.originalFilename;
    var filename = req.params.filename;
    var tempFile = file.path;
    //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destFile = 'logo' + path.sep + req.params.id + '_' + filename;

    // hipparca-sport.s3.amazonaws.com
    var stream = fs.createReadStream(file.path);
    s3fsImpl.writeFile(destFile, stream).then(function () {
      console.log('s3fs ok');
      fs.unlink(file.path, function (err) {
        if (err) {
          console.error(err);
        }
      });

      Sportstaette.findById(req.params.id, function (err, sportstaette) {
        if (err) {
          return handleError(res, err);
        }
        if (!sportstaette) {
          return res.sendStatus(404);
        }

        sportstaette.logo = filename;

        sportstaette.save(function (err) {
          if (err) {
            console.log('save error: ' + err);
            return handleError(res, err);
          }
          console.log('logo (' + filename + ') uploaded and saved');
        });
        //return res.json(200, objekt);
        return res.status(200).end();
      });
      //res.status(200).end();
    });
  });

};



// Image upload
exports.uploadImageOld = function(req, res) {
    console.log('uploading to ' + req.params.id);
    var tempPath = 'temp/';

    if (fs.existsSync(tempPath)===false) {
        fs.mkdirSync(tempPath,'0755');
    }

    var form = new multiparty.Form({
        autoFiles: true,
        uploadDir: '.' + path.sep + tempPath
    });

    form.parse(req, function(err, fields, files) {
        if (err) { return handleError(res, err); }

        var file = files.file[0];
        //var contentType = file.headers['content-type'];
        //var extension = file.path.substring(file.path.lastIndexOf('.'));
        var filename = file.originalFilename;
        var tempFile = file.path;
        //var destPath = 'uploads' + path.sep + req.params.id;
        //var destFile = destPath + path.sep + filename;
        var destPath = 'uploads' + path.sep + 'image';
        var destFile = destPath + path.sep + req.params.id + '_' + filename;

        var dirs = destPath.split(path.sep);
        var newDir = '';
        //console.log(__dirname);
        for (var i = 0; i < dirs.length; i++) {
            newDir += (i>0) ? path.sep : '';
            newDir += dirs[i];
            console.log(newDir);

            if (fs.existsSync(newDir)===false) {
                fs.mkdirSync(newDir,'0755');
                console.log('mkdir '+newDir);
            }
        }

        //console.log(file);
        //console.log(contentType);
        //console.log(filename);
        //console.log(tempFile);
        //console.log(destFile);

        // copy file to dir
        fs.rename(tempFile, destFile, function(err) {
            if(err) {
                console.error('rename error: '+err.stack);
                return handleError(res, err);
            } else {
                Sportstaette.findById(req.params.id, function (err, sportstaette) {
                    //console.log( filename + ': ' + objekt.__v)
                    if (err) { return handleError(res, err); }
                    if (!sportstaette) { return res.sendStatus(404); }

                    sportstaette.bilder.push(filename);

                    sportstaette.save(function (err) {
                        if (err) {  console.log('save error: ' + err); return handleError(res, err); }
                        console.log('image (' + filename + ') uploaded and saved');
                    });
                    //return res.json(200, objekt);
                    return res.status(200).end();;
                });
            }
        });

        //return;
    });

};

// Image upload S3
exports.uploadImage = function (req, res) {
  console.log('uploading (' + req.params.filename + ') to ' + req.params.id);
  var tempPath = 'temp/';

  if (fs.existsSync(tempPath) === false) {
    fs.mkdirSync(tempPath, '0755');
  }

  var form = new multiparty.Form({
    autoFiles: true,
    uploadDir: '.' + path.sep + tempPath
  });

  form.parse(req, function (err, fields, files) {
    if (err) {
      return handleError(res, err);
    }

    var file = files.file[0];
    //var filename = file.originalFilename;
    var filename = req.params.filename;
    var tempFile = file.path;
    //console.log(process.env.OPENSHIFT_DATA_DIR);
    var destFile = 'image' + path.sep + req.params.id + '_' + filename;

    // hipparca-sport.s3.amazonaws.com
    var stream = fs.createReadStream(file.path);
    s3fsImpl.writeFile(destFile, stream).then(function () {
      console.log('s3fs ok');
      fs.unlink(file.path, function (err) {
        if (err) {
          console.error(err);
        }
      });

      Sportstaette.findById(req.params.id, function (err, sportstaette) {
        if (err) {
          return handleError(res, err);
        }
        if (!sportstaette) {
          return res.sendStatus(404);
        }

        sportstaette.bilder.push(filename);
        //console.log(filename);

        sportstaette.save(function (err) {
          if (err) {
            console.log('save error: ' + err);
            return handleError(res, err);
          }
          console.log('image (' + filename + ') uploaded and saved');
        });
        //return res.json(200, objekt);
        return res.status(200).end();
      });
      //res.status(200).end();
    });
  });

};

function handleError(res, err) {
  return res.status(500).send(err);
}
