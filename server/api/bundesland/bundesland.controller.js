'use strict';

var _ = require('lodash');
var Bundesland = require('./bundesland.model');
var ObjectId = require('mongoose').Types.ObjectId;

// Get list of bundeslands
exports.index = function (req, res) {
  /*Bundesland.find(function (err, bundeslands) {
   if (err) {
   return handleError(res, err);
   }
   return res.json(200, bundeslands);
   return res.status(200).json(bundeslands);
   });*/
  Bundesland.find({})
  //.populate('land')
    .exec(function (err, bundeslands) {
      if (err) {
        return handleError(res, err);
      }
      //return res.json(200, bundeslands);
      return res.status(200).json(bundeslands);
    });
};

// Get list of bundeslands by country
exports.getByLand = function (req, res) {
  //console.log(req.params.name);
  if (req.body.land && req.body.land != '') {
    var query = { land: req.body.land };
  } else {
    var query = {};
  }

  //console.log(query);
  Bundesland.find(query)
    .populate('land')
    .exec(function (err, bundeslands) {
      if (err) {
        return handleError(res, err);
      }
      //console.log(bundeslands);
      //console.log(req.params.name);
      if (req.params.name && req.params.name != '') {
        //var bl = _.find(bundeslands, { 'land': { 'name': req.params.name } });
      }
      //return res.json(200, bundeslands);
      return res.status(200).json(bundeslands);
    });
};

// Get a single bundesland
exports.show = function (req, res) {
  /*Bundesland.findById(req.params.id, function (err, bundesland) {
   if (err) {
   return handleError(res, err);
   }
   if (!bundesland) {
   return res.sendStatus(404);
   }
   return res.json(bundesland);
   });*/
  Bundesland.findById(req.params.id)
  //.populate('land')
    .exec(function (err, bundesland) {
      if (err) {
        return handleError(res, err);
      }
      if (!bundesland) {
        return res.sendStatus(404);
      }
      return res.json(bundesland);
      //return res.status(200).json(bundesland);
    });
};

// Creates a new bundesland in the DB.
exports.create = function (req, res) {
  Bundesland.create(req.body, function (err, bundesland) {
    if (err) {
      return handleError(res, err);
    }
    //return res.json(201, bundesland);
    return res.status(201).json(bundesland);
  });
};

// Updates an existing bundesland in the DB.
exports.update = function (req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Bundesland.findById(req.params.id, function (err, bundesland) {
    if (err) {
      return handleError(res, err);
    }
    if (!bundesland) {
      return res.sendStatus(404);
    }
    var updated = _.merge(bundesland, req.body);
    updated.save(function (err) {
      if (err) {
        return handleError(res, err);
      }
      //return res.json(200, bundesland);
      return res.status(200).json(bundesland);
    });
  });
};

// Deletes a bundesland from the DB.
exports.destroy = function (req, res) {
  Bundesland.findById(req.params.id, function (err, bundesland) {
    if (err) {
      return handleError(res, err);
    }
    if (!bundesland) {
      return res.sendStatus(404);
    }
    bundesland.remove(function (err) {
      if (err) {
        return handleError(res, err);
      }
      return res.sendStatus(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
