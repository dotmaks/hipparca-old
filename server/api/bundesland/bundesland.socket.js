/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Bundesland = require('./bundesland.model');

exports.register = function(socket) {
  Bundesland.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Bundesland.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('bundesland:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('bundesland:remove', doc);
}