'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BundeslandSchema = new Schema({
    name:                        String,
    land:                        { type: Schema.Types.ObjectId, ref: 'Land' },
    kennziffer:                  Number,
    active:                      { type: Boolean, default: true },
});

module.exports = mongoose.model('Bundesland', BundeslandSchema);
