'use strict';

var _ = require('lodash');
var Protokoll = require('./protokoll.model');

// Get list of protokolle
exports.index = function(req, res) {
  Protokoll.find(function (err, protokolle) {
    if(err) { return handleError(res, err); }
    //return res.json(200, protokolle);
    return res.status(200).json(protokolle);
  });
};

// Get a single protokoll
exports.show = function(req, res) {
  Protokoll.findById(req.params.id, function (err, protokoll) {
    if(err) { return handleError(res, err); }
    if(!protokoll) { return res.sendStatus(404); }
    return res.json(protokoll);
    //return res.status(200).json(protokoll);
  });
};

// Creates a new protokoll in the DB.
exports.create = function(req, res) {
  Protokoll.create(req.body, function(err, protokoll) {
    if(err) { return handleError(res, err); }
    //return res.json(201, protokoll);
    return res.status(201).json(protokoll);
  });
};

// Updates an existing protokoll in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Protokoll.findById(req.params.id, function (err, protokoll) {
    if (err) { return handleError(res, err); }
    if(!protokoll) { return res.sendStatus(404); }
    var updated = _.merge(protokoll, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, protokoll);
      return res.status(200).json(protokoll);
    });
  });
};

// Deletes a protokoll from the DB.
exports.destroy = function(req, res) {
  Protokoll.findById(req.params.id, function (err, protokoll) {
    if(err) { return handleError(res, err); }
    if(!protokoll) { return res.sendStatus(404); }
    protokoll.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
