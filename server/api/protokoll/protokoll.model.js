'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ProtokollSchema = new Schema({
    text:                        String,
    datum:                       {type: Date, default: Date.now},

    active:                      {type: Boolean, default: true},
});

module.exports = mongoose.model('Protokoll', ProtokollSchema);
