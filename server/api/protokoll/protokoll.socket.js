/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Protokoll = require('./protokoll.model');

exports.register = function(socket) {
  Protokoll.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Protokoll.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('protokoll:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('protokoll:remove', doc);
}
