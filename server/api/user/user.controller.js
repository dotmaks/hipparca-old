'use strict';

var User = require('./user.model');
var passport = require('passport');
var config = require('../../config/environment');
var jwt = require('jsonwebtoken');
var nodemailer = require('nodemailer');
var generator = require('generate-password');

var Verein = require('../verein/verein.model');
var Shop = require('../shop/shop.model');
var Sportstaette = require('../sportstaette/sportstaette.model');
var Termin = require('../termin/termin.model');

var validationError = function (res, err) {
  return res.status(422).json(err);
};

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
  User.find({}, '-salt -hashedPassword', function (err, users) {
    if (err) return res.status(500).send(err);
    res.status(200).json(users);
  });
};

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  var newPassword = generator.generate({
    length: 10,
    numbers: true
  });
  console.log(newPassword);

  User.findOne({ email: req.body.email })
    .exec(function (err, user) {
      if (err) {
        return handleError(res, err);
      }

      if (user) {
        console.log('user already registered');
        user.password = newPassword;
        user.save(function (err) {
          if (err) return validationError(res, err);
          var token = jwt.sign({ _id: user._id }, config.secrets.session, { expiresIn: 60 * 60 });
          //res.json({ token: token });
          res.status(200).json({ user: user, newPassword: newPassword });
        });
      } else {
        console.log('no user registered, create new user');
        req.body.name = req.body.email;
        req.body.password = newPassword;
        var newUser = new User(req.body);
        newUser.provider = 'local';
        newUser.role = 'user';
        newUser.save(function (err, user) {
          if (err) return validationError(res, err);
          var token = jwt.sign({ _id: user._id }, config.secrets.session, { expiresIn: 60 * 60 });
          //res.json({ token: token });
          res.status(200).json({ user: newUser, newPassword: newPassword });
        });
      }
    });
};

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id;

  User.findById(userId, function (err, user) {
    if (err) return next(err);
    if (!user) return res.sendStatus(401);
    res.json(user.profile);
  });
};

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return res.status(500).send(err);
    return res.sendStatus(204);
  });
};

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
  var userId = req.user._id;
  var newName = String(req.body.name);
  var oldPass = String(req.body.oldPassword);
  var newPass = String(req.body.newPassword);

  User.findById(userId, function (err, user) {
    if (user.authenticate(oldPass)) {
      user.name = newName;
      user.password = newPass;
      user.save(function (err) {
        if (err) return validationError(res, err);
        res.sendStatus(200);
      });
    } else {
      res.sendStatus(403);
    }
  });
};

/**
 * Change a users role and status
 */
exports.updateUser = function (req, res, next) {
  var userId = req.body._id;
  var newName = String(req.body.name);
  var newRole = String(req.body.role);
  var newActive = String(req.body.active);

  User.findById(userId, function (err, user) {
    user.name = newName;
    user.role = newRole;
    user.active = newActive;
    user.save(function (err) {
      if (err) return validationError(res, err);
      res.sendStatus(200);
    });
  });
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id;
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
    if (err) return next(err);
    //if (!user) return res.json(401);
    if (!user) return res.status(401);
    res.json(user);
  });
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
  res.redirect('/');
};

/**
 * Sends user mail update request
 */
exports.mailNewPassword = function (req, res) {
  var smtpConfig = {
    host: 'sslout.df.eu',
    port: 465,
    secure: true, // use SSL
    auth: {
      user: 'office@hipparca.at',
      pass: 'ezy52U5H#tX-'
    }
  };

  var transporter = nodemailer.createTransport(smtpConfig);

  var text = 'Hallo ' + req.body.user.name + '!'
           + '\n\n'
           + 'Ihr neues Passwort lautet: ' + req.body.newPassword
           + '\n\n\n'
           + 'Mit freundlichen Grüßen - Ihr hipparca Team'

  var mailOptions = {
    from: 'office@hipparca.at', // sender address
    to: req.body.user.email, // list of receivers
    subject: 'Ihr neues Passwort', // Subject line
    text: text //, // plaintext body
    // html: '<b>Hello world </b>' // You can choose to send an HTML body instead
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
      res.json({ yo: 'error' });
    } else {
      console.log('Message sent: ' + info.response);
      res.json({ yo: info.response });
    }
  });
};


/**
 * Check if email exists
 */
exports.checkMail = function(req, res) {
  if (req.body.email && (req.body.email != '')) {
    var query = { $and: [ { email: { $exists: true, $ne: null } }, { email: req.body.email } ] };
  } else {
    return handleError(res, null);
  }

  var count = 0;

  User.findOne(query, function (err, user) {
    if (!user) {
      // first search vereine
      Verein.find(query)
        .exec(function (err, vereine) {
          if (err) {
            return handleError(res, err);
          }

          count += vereine.length;
          //console.log('vereine ' + vereine.length);

          if (count===0) {
            // second search termine
            Shop.find(query)
              .exec(function (err, shops) {
                if (err) {
                  return handleError(res, err);
                }

                count += shops.length;
                //console.log('shops ' + shops.length);

                if (count===0) {
                  // third search sportstaetten
                  Sportstaette.find(query)
                    .exec(function (err, sportstaetten) {
                      if (err) {
                        return handleError(res, err);
                      }

                      count += sportstaetten.length;
                      //console.log('sportstaetten ' + sportstaetten.length);

                      if (count===0) {
                        // fourth search termine
                        Termin.find(query)
                          .exec(function (err, termine) {
                            if (err) {
                              return handleError(res, err);
                            }

                            count += termine.length;
                            //console.log('termine ' + termine.length);

                            if (count===0) {
                              return res.status(200).json({
                                success: false,
                                email: req.body.email
                              });
                            } else {
                              return res.status(200).json({
                                success: true,
                                email: req.body.email
                              });
                            }
                          });
                      } else {
                        return res.status(200).json({
                          success: true,
                          email: req.body.email
                        });
                      }
                    });
                } else {
                  return res.status(200).json({
                    success: true,
                    email: req.body.email
                  });
                }
              });
          } else {
            return res.status(200).json({
              success: true,
              email: req.body.email
            });
          }
        });
    } else {
      return res.status(200).json({
        success: true,
        email: req.body.email
      });
    }
  });
};



function handleError(res, err) {
  return res.status(500).send(err);
}
