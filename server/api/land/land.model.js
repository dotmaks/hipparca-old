'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var LandSchema = new Schema({
    name:            String,
    active:          { type: Boolean, default: true },
});

module.exports = mongoose.model('Land', LandSchema);
