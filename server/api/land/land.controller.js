'use strict';

var _ = require('lodash');
var Land = require('./land.model');

// Get list of laender
exports.index = function(req, res) {
  Land.find(function (err, laender) {
    if(err) { return handleError(res, err); }
    //return res.json(200, laender);
    return res.status(200).json(laender);
  });
};

// Get a single land
exports.show = function(req, res) {
  Land.findById(req.params.id, function (err, land) {
    if(err) { return handleError(res, err); }
    if(!land) { return res.sendStatus(404); }
    return res.json(land);
    //return res.status(200).json(land);
  });
};

// Creates a new land in the DB.
exports.create = function(req, res) {
  Land.create(req.body, function(err, land) {
    if(err) { return handleError(res, err); }
    //return res.json(201, land);
    return res.status(201).json(land);
  });
};

// Updates an existing land in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Land.findById(req.params.id, function (err, land) {
    if (err) { return handleError(res, err); }
    if(!land) { return res.sendStatus(404); }
    var updated = _.merge(land, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, land);
      return res.status(200).json(land);
    });
  });
};

// Deletes a land from the DB.
exports.destroy = function(req, res) {
  Land.findById(req.params.id, function (err, land) {
    if(err) { return handleError(res, err); }
    if(!land) { return res.sendStatus(404); }
    land.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
