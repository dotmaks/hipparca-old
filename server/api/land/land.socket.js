/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Land = require('./land.model');

exports.register = function(socket) {
  Land.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Land.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('land:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('land:remove', doc);
}
