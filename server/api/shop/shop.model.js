'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ShopSchema = new Schema({
    name:                        String,

    protokoll:                   String,
    kurzbeschreibung:            String,

    sportarten:                  [{ type: Schema.Types.ObjectId, ref: 'Sportart' }],

    logo:                        String,
    strasse :                    String,
    plz:                         String,
    ort:                         String,
    bundesland:                  { type: Schema.Types.ObjectId, ref: 'Bundesland' },
    land:                        { type: Schema.Types.ObjectId, ref: 'Land' },
    koordLaenge:                 String,
    koordBreite:                 String,
    kontaktperson:               String,
    telefon:                     String,
    email:                       String,
    url:                         String,

    realshop:                    { type: Boolean, default: false },
    webshop:                     { type: Boolean, default: false },
    shopkategorien:              [{ type: Schema.Types.ObjectId, ref: 'Shopkategorie' }],

    oeffnungszeiten:             String,
    eigentext:                   String,
    bilder:                      [String],

    active:                      { type: Boolean, default: false },
    _created:                    { type: Date, default: Date.now },
    _createdby:                  { type: Schema.Types.ObjectId, ref: 'User' },
    _lastupdate:                 { type: Date },
    _lastupdateby:               { type: Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Shop', ShopSchema);
