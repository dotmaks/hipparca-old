/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Sportart = require('./sportart.model');

exports.register = function(socket) {
  Sportart.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Sportart.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('sportart:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('sportart:remove', doc);
}