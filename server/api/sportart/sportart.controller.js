'use strict';

var _ = require('lodash');
var Sportart = require('./sportart.model');

// Get list of sportarts
exports.index = function(req, res) {
  Sportart.find(function (err, sportarts) {
    if(err) { return handleError(res, err); }
    //return res.json(200, sportarts);
    return res.status(200).json(sportarts);
  });
};

// Get a single sportart
exports.show = function(req, res) {
  Sportart.findById(req.params.id, function (err, sportart) {
    if(err) { return handleError(res, err); }
    if(!sportart) { return res.sendStatus(404); }
    //return res.json(sportart);
    return res.status(200).json(sportart);
  });
};

// Get a single sportart
exports.seo = function(req, res) {
  Sportart.find(function (err, sportarten) {
    if(err) { return handleError(res, err); }
    //var sportart = _.findWhere(sportarten, { 'seo' : req.params.seo });
    var sportart = _.find(sportarten, { 'seo' : req.params.seo });
    if(!sportart) { return res.sendStatus(404); }
    //return res.json(sportart);
    return res.status(200).json(sportart);
  });
};

// Creates a new sportart in the DB.
exports.create = function(req, res) {
  Sportart.create(req.body, function(err, sportart) {
    if(err) { return handleError(res, err); }
    //return res.json(201, sportart);
    return res.status(201).json(sportart);
  });
};

// Updates an existing sportart in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Sportart.findById(req.params.id, function (err, sportart) {
    if (err) { return handleError(res, err); }
    if(!sportart) { return res.sendStatus(404); }
    var updated = _.merge(sportart, req.body);
    console.log(updated);
    updated.markModified('kategorien'); // lets mongoose know these values changed
    console.log(updated);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, sportart);
      return res.status(200).json(sportart);
    });
  });
};

// Deletes a sportart from the DB.
exports.destroy = function(req, res) {
  Sportart.findById(req.params.id, function (err, sportart) {
    if(err) { return handleError(res, err); }
    if(!sportart) { return res.sendStatus(404); }
    sportart.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
