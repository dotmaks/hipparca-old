'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SportartSchema = new Schema({
    name:                        String,
    seo:                         String,
    //kategorien:                  [String],
    hauptkategorie:              { type : Schema.Types.ObjectId, ref: 'Sportart' },
    isHauptkategorie:            { type: Boolean, default: false },
    active:                      { type: Boolean, default: true },
});

module.exports = mongoose.model('Sportart', SportartSchema);
