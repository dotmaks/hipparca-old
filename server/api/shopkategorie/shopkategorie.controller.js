'use strict';

var _ = require('lodash');
var Shopkategorie = require('./shopkategorie.model.js');

// Get list of shopkategorie
exports.index = function(req, res) {
  /*Shopkategorie.find(function (err, shopkategorie) {
    if(err) { return handleError(res, err); }
    return res.json(200, shopkategorie);
    return res.status(200).json(shopkategorie);
  });*/
  Shopkategorie.find({})
    //.populate( 'sportart' )
    .exec( function (err, shopkategorie) {
      if(err) { return handleError(res, err); }
      //return res.json(200, shopkategorie);
      return res.status(200).json(shopkategorie);
    } );
};

// Get a single shopkategorie
exports.show = function(req, res) {
  /*Shopkategorie.findById(req.params.id, function (err, shopkategorie) {
    if(err) { return handleError(res, err); }
    if(!shopkategorie) { return res.sendStatus(404); }
    return res.json(shopkategorie);
  });*/
  Shopkategorie.findById(req.params.id)
    .populate( 'sportart' )
    .exec( function (err, shopkategorie) {
      if(err) { return handleError(res, err); }
      if(!shopkategorie) { return res.sendStatus(404); }
      return res.json(shopkategorie);
      //return res.status(200).json(shopkategorie);
    });
};

// Creates a new shopkategorie in the DB.
exports.create = function(req, res) {
  //console.log(req.body);

  //if (typeof req.body.sportart !== 'undefined' && req.body.sportart !== null && typeof req.body.sportart._id !== 'undefined') { req.body.sportart = req.body.sportart._id; }

  Shopkategorie.create(req.body, function(err, shopkategorie) {
    if(err) { return handleError(res, err); }
    //return res.json(201, shopkategorie);
    return res.status(201).json(shopkategorie);
  });
};

// Updates an existing shopkategorie in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Shopkategorie.findById(req.params.id, function (err, shopkategorie) {
    if (err) { return handleError(res, err); }
    if (!shopkategorie) { return res.sendStatus(404); }

    //if (typeof req.body.sportart !== 'undefined' && req.body.sportart !== null && typeof req.body.sportart._id !== 'undefined') { req.body.sportart = req.body.sportart._id; }

    var updated = _.merge(shopkategorie, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      //return res.json(200, shopkategorie);
      return res.status(200).json(shopkategorie);
    });
  });
};

// Deletes a shopkategorie from the DB.
exports.destroy = function(req, res) {
  Shopkategorie.findById(req.params.id, function (err, shopkategorie) {
    if(err) { return handleError(res, err); }
    if(!shopkategorie) { return res.sendStatus(404); }
    shopkategorie.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.sendStatus(204);
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
