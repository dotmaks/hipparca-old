/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Shopkategorie = require('./shopkategorie.model.js');

exports.register = function(socket) {
  Shopkategorie.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Shopkategorie.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('shopkategorie:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('shopkategorie:remove', doc);
}