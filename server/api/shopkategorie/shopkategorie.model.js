'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var ShopkategorieSchema = new Schema({
    name:            String,
    sportart:        { type: Schema.Types.ObjectId, ref: 'Sportart' },
    active:          { type: Boolean, default: true },
});

module.exports = mongoose.model('Shopkategorie', ShopkategorieSchema);
